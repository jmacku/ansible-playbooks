ins-gen
=======

Installs epel repo, updates all packakes and installs list of extra packages.

Role Variables
--------------

ins_gen_packages
  List of packages to install. Default list is part of the role.

License
-------

BSD
