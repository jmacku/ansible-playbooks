add-user
========

Prepares the user accout to be used instad of root. For any subsequent tasks the created user can be used.

Requirements
------------

This role has to be run under root account.

Role Variables
--------------

user_name
  name of the user account

user_pswd
  hash of user password - do not use plain text password here

There are default values for both variables provided.

Dependencies
------------

No dependecies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: add-user
           vars:
             user_name: "{{ user_name }}"
             user_pswd: "{{ user_pswd }}"

License
-------

BSD
